<?php

declare(strict_types=1);

namespace App\Services;

use App\Transformers\MovieTransformer;
use App\Transformers\ShowTransformer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response as Psr7Response;

final class TraktApiService
{
    private const SEARCH_SHOW = '/search/show?extended=full&query=%s';

    private const SHOW_TRAKTID = '/shows/%d?extended=full';

    private const SEARCH_MOVIE = '/search/movie?extended=full&query=%s';

    private const MOVIE_TRAKTID = '/movies/%d?extended=full';

    public function __construct()
    {
        $this->api = new Client([
            'base_uri' => 'https://api.trakt.tv',
            'headers' => [
                'content-type' => 'application/json',
                'trakt-api-version' => 2,
                'trakt-api-key' => env('TRAKT_API_CLIENTID'),
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public static function searchShow(string $search)
    {
        $url = sprintf(static::SEARCH_SHOW, $search);

        $api = new self();

        try {
            $result = $api->getResults($url);
        } catch (RequestException $e) {
            die($e->getMessage());
        }

        $shows = $api->returnJson($result);

        return fractal($shows, new ShowTransformer())->respond();
    }

    public static function searchShowForArticle(string $search)
    {
        $url = sprintf(static::SEARCH_SHOW, $search);

        $api = new self();

        return $api->searchForArticle($url);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public static function searchMovie(string $search)
    {
        $url = sprintf(static::SEARCH_MOVIE, $search);

        $api = new self();

        try {
            $result = $api->getResults($url);
        } catch (RequestException $e) {
            die($e->getMessage());
        }

        $movies = $api->returnJson($result);

        return fractal($movies, new MovieTransformer())->respond();
    }

    public static function searchMovieForArticle(string $search)
    {
        $url = sprintf(static::SEARCH_MOVIE, $search);

        $api = new self();

        return $api->searchForArticle($url);
    }

    /**
     * @param $traktId
     */
    public static function getMovieByTraktId($traktId)
    {
        $url = sprintf(static::MOVIE_TRAKTID, $traktId);

        $api = new self();

        try {
            $result = $api->getResults($url);
        } catch (RequestException $e) {
            die($e->getMessage());
        }

        return $api->returnJson($result);
    }

    /**
     * @param $traktId
     */
    public static function getShowByTraktId($traktId)
    {
        $url = sprintf(static::SHOW_TRAKTID, $traktId);

        $api = new self();

        try {
            $result = $api->getResults($url);
        } catch (RequestException $e) {
            die($e->getMessage());
        }

        return $api->returnJson($result);
    }

    /**
     * @param $url
     */
    private function searchForArticle($url)
    {
        try {
            $result = $this->getResults($url);
        } catch (RequestException $e) {
            die($e->getMessage());
        }

        return $this->returnJson($result);
    }

    /**
     * @param $url
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function getResults($url)
    {
        try {
            $request = $this->api->get($url);
        } catch (ClientException $e) {
        } catch (ServerException $e) {
        } catch (ConnectException $e) {
        }

        return $request;
    }

    private function returnJson(Psr7Response $response)
    {
        return json_decode($response->getBody()->getContents());
    }
}
