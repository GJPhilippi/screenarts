<?php

declare(strict_types=1);

namespace App\Services;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

final class TmdbApiService
{
    public const MOVIE_URL = 'movie/%u';

    public const SHOW_URL = 'tv/%u';

    public function __construct()
    {
        $this->api = new Client([
            'base_uri' => 'https://api.themoviedb.org/3/',
            'headers' => [
                'Authorization' => 'Bearer ' . env('TMDB_API_BEARER'),
                'Content-Type' => 'application/json;charset=utf-8',
            ],
        ]);
    }

    /**
     * @param $uri
     *
     * @return ResponseInterface
     */
    public function getRequest($uri, array $requestOptions = [])
    {
        $options = ['query' => [
            'language' => 'nl',
        ]];

        $options = array_merge_recursive($options, $requestOptions);

        try {
            $request = $this->api->get($uri, $options);
        } catch (ClientException $e) {
        } catch (ServerException $e) {
        } catch (ConnectException $e) {
        }

        return $request;
    }

    /**
     * @param $id
     */
    public static function getMovieByTmdbId($id)
    {
        $api = new self();
        $url = sprintf(static::MOVIE_URL, $id);

        $result = $api->getRequest($url);

        if ($result->getStatusCode() === 200) {
            return json_decode($result->getBody()->getContents());
        }
    }

    /**
     * @param $id
     */
    public static function getShowByTmdbId($id)
    {
        $api = new self();
        $url = sprintf(static::SHOW_URL, $id);

        $result = $api->getRequest($url);

        if ($result->getStatusCode() === 200) {
            return json_decode($result->getBody()->getContents());
        }
    }
}
