<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Movie;
use App\Services\TmdbApiService;
use App\Services\TraktApiService;
use Illuminate\Database\Eloquent\Collection;

final class MovieRepository
{
    public function __construct()
    {
        $this->genreRepository = new GenreRepository();
    }

    public function createMovie($traktMovieId): Movie
    {
        $movieInfo = TraktApiService::getMovieByTraktId($traktMovieId);

        $movie = Movie::firstOrCreate([
            'id_trakt' => $movieInfo->ids->trakt,
        ]);

        if ($movie->wasRecentlyCreated) {
            $movie->title = $movieInfo->title;
            $movie->tagline = isset($movieInfo->tagline) ? $movieInfo->tagline : null;
            $movie->status = isset($movieInfo->status) ? $movieInfo->status : null;
            $movie->country = isset($movieInfo->country) ? mb_strtoupper($movieInfo->country) : null;
            $movie->runtime = isset($movieInfo->runtime) ? $movieInfo->runtime : null;
            $movie->trailer = isset($movieInfo->trailer) ? $movieInfo->trailer : null;
            $movie->id_imdb = isset($movieInfo->ids->imdb) ? $movieInfo->ids->imdb : null;
            $movie->slug = $movieInfo->ids->slug;

            if (isset($movieInfo->ids->tmdb)) {
                $movie->id_tmdb = $movieInfo->ids->tmdb;
                $tmdbInfo = $this->getTmdbInfo($movieInfo->ids->tmdb);

                $movie->overview = isset($tmdbInfo->overview) ? $tmdbInfo->overview : null;
                $movie->released_at = $tmdbInfo->release_date;

                $genres = $this->genreRepository->createGenres($tmdbInfo->genres);
                $movie->genres()->sync($genres);

                $this->processImages($tmdbInfo->poster_path);
            }

            $movie->save();
        }

        return $movie;
    }

    public function createMovies(array $movies): array
    {
        $relatedMovies = [];

        foreach ($movies as $movie) {
            $newMovie = $this->createMovie($movie);
            $relatedMovies[] = $newMovie->id;
        }

        return $relatedMovies;
    }

    public function getSlimSelect(Collection $movies): array
    {
        $articleMovies = [];

        foreach ($movies as $movie) {
            $articleMovies[] = [
                'text' => $movie->title,
                'value' => $movie->id_trakt,
            ];
        }

        return $articleMovies;
    }

    private function getTmdbInfo($tmdbId)
    {
        return TmdbApiService::getMovieByTmdbId($tmdbId);
    }

    private function processImages(): bool
    {
        return true;
    }
}
