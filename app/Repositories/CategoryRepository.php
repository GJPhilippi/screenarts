<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Category;
use Illuminate\Database\Eloquent\Collection;

final class CategoryRepository
{
    public function createCategory($category): Category
    {
        return Category::firstOrCreate([
            'title' => $category,
        ]);
    }

    public function createCategories(array $categories): array
    {
        $relatedCategories = [];

        foreach ($categories as $category) {
            $newCategory = $this->createCategory($category);
            $relatedCategories[] = $newCategory->id;
        }

        return $relatedCategories;
    }

    public function getSlimSelect(Collection $categories): array
    {
        $articleCategories = [];

        foreach ($categories as $category) {
            $articleCategories[] = [
                'text' => $category->title,
            ];
        }

        return $articleCategories;
    }
}
