<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Tag;
use Illuminate\Database\Eloquent\Collection;

final class TagRepository
{
    public function createTag($type): Tag
    {
        return Tag::firstOrCreate([
            'title' => $type,
        ]);
    }

    public function createTags(array $tags): array
    {
        $relatedTags = [];

        foreach ($tags as $tag) {
            $newTag = $this->createTag($tag);
            $relatedTags[] = $newTag->id;
        }

        return $relatedTags;
    }

    public function getSlimSelect(Collection $tags): array
    {
        $articleTags = [];

        foreach ($tags as $tag) {
            $articleTags[] = [
                'text' => $tag->title,
            ];
        }

        return $articleTags;
    }
}
