<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Article;
use Carbon\Carbon;
use Purifier;
use Str;

final class ArticleRepository
{
    /**
     * @param $title
     * @param $published
     * @param $published_at
     * @param $content
     */
    public function createArticle($title, $published, $published_at, $content): Article
    {
        return Article::create([
            'title' => $title,
            'published' => $published,
            'published_at' => Carbon::parse($published_at)->format('Y-m-d H:i:s'),
            'slug' => Str::slug($title, '-'),
            'content' => Purifier::clean($content, 'youtube'),
        ]);
    }
}
