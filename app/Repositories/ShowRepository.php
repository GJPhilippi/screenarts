<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Services\TmdbApiService;
use App\Services\TraktApiService;
use App\Show;
use Illuminate\Database\Eloquent\Collection;

final class ShowRepository
{
    public function __construct()
    {
        $this->genreRepository = new GenreRepository();
    }

    public function createShow($traktShowId): Show
    {
        $showInfo = TraktApiService::getShowByTraktId($traktShowId);

        $show = Show::firstOrCreate([
            'id_trakt' => $showInfo->ids->trakt,
        ]);

        if ($show->wasRecentlyCreated) {
            $show->title = $showInfo->title;
            $show->trailer = isset($showInfo->trailer) ? $showInfo->trailer : null;
            $show->network = isset($showInfo->network) ? $showInfo->network : null;
            $show->country = isset($showInfo->country) ? $showInfo->country : null;
            $show->episode_runtime = isset($showInfo->runtime) ? $showInfo->runtime : null;
            $show->airs = isset($showInfo->airs) ? $showInfo->airs : null;
            $show->year = isset($showInfo->year) ? $showInfo->year : null;
            $show->status = isset($showInfo->status) ? $showInfo->status : null;
            $show->id_trakt = isset($showInfo->ids->trakt) ? $showInfo->ids->trakt : null;
            $show->id_tvdb = isset($showInfo->ids->tvdb) ? $showInfo->ids->tvdb : null;
            $show->id_trakt = isset($showInfo->ids->trakt) ? $showInfo->ids->trakt : null;
            $show->id_tvrage = isset($showInfo->ids->tvrage) ? $showInfo->ids->tvrage : null;
            $show->id_imdb = isset($showInfo->ids->imdb) ? $showInfo->ids->imdb : null;
            $show->slug = $showInfo->ids->slug;

            if (isset($showInfo->ids->tmdb)) {
                $show->id_tmdb = $showInfo->ids->tmdb;
                $tmdbInfo = $this->getTmdbInfo($showInfo->ids->tmdb);
                $show->first_aired = $tmdbInfo->first_air_date;
                $show->overview = isset($tmdbInfo->overview) ? $tmdbInfo->overview : null;

                $genres = $this->genreRepository->createGenres($tmdbInfo->genres);
                $show->genres()->sync($genres);

                $this->processImages($tmdbInfo->poster_path);
            }

            $show->save();
        }

        return $show;
    }

    public function createShows(array $shows): array
    {
        $relatedShows = [];

        foreach ($shows as $show) {
            $newShow = $this->createShow($show);
            $relatedShows[] = $newShow->id;
        }

        return $relatedShows;
    }

    public function getSlimSelect(Collection $shows): array
    {
        $articleShows = [];

        foreach ($shows as $show) {
            $articleShows[] = [
                'text' => $show->title,
                'value' => $show->id_trakt,
            ];
        }

        return $articleShows;
    }

    private function getTmdbInfo($tmdbId)
    {
        return TmdbApiService::getShowByTmdbId($tmdbId);
    }

    private function processImages(): bool
    {
        return true;
    }
}
