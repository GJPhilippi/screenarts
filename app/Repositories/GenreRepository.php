<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Genre;

final class GenreRepository
{
    public function createGenre($genre): Genre
    {
        return Genre::firstOrCreate([
            'genre' => $genre,
        ]);
    }

    public function createGenres(array $genres): array
    {
        $relatedGenres = [];

        foreach ($genres as $genre) {
            $newGenre = $this->createGenre($genre->name);
            $relatedGenres[] = $newGenre->id;
        }

        return $relatedGenres;
    }
}
