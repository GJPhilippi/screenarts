<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Type;
use Illuminate\Database\Eloquent\Collection;

final class TypeRepository
{
    public function createType($type): Type
    {
        return Type::firstOrCreate([
            'title' => $type,
        ]);
    }

    public function createTypes(array $types): array
    {
        $relatedTypes = [];

        foreach ($types as $type) {
            $newType = $this->createType($type);
            $relatedTypes[] = $newType->id;
        }

        return $relatedTypes;
    }

    public function getSlimSelect(Collection $types): array
    {
        $articleTypes = [];

        foreach ($types as $type) {
            $articleTypes[] = [
                'title' => $type->title,
            ];
        }

        return $articleTypes;
    }
}
