<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Genre extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'genre',
    ];
}
