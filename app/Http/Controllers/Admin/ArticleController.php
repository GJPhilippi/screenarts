<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreArticleRequest;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\MovieRepository;
use App\Repositories\ShowRepository;
use App\Repositories\TagRepository;
use App\Repositories\TypeRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

final class ArticleController extends Controller
{
    public function __construct()
    {
        $this->movieRepository = new MovieRepository();
        $this->showRepository = new ShowRepository();
        $this->categoryRepository = new CategoryRepository();
        $this->tagRepository = new TagRepository();
        $this->articleRepository = new ArticleRepository();
        $this->typeRepository = new TypeRepository();
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $view = view('admin.article.index');
        $view->articles = Article::latest()->take(50)->get();

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreArticleRequest $request): RedirectResponse
    {
        $title = $request->get('title');
        $movies = $request->get('movies');
        $shows = $request->get('shows');
        $categories = $request->get('categories');
        $tags = $request->get('tags');
        $types = $request->get('types');
        $published = $request->get('published') === 1;
        $published_at = $request->get('published_at');
        $content = $request->get('content');

        $article = $this->articleRepository->createArticle($title, $published, $published_at, $content);

        if (is_countable($movies) && count($movies) > 0) {
            $relatedMovies = $this->movieRepository->createMovies($movies);
            $article->movies()->sync($relatedMovies);
        }

        if (is_countable($shows) && count($shows) > 0) {
            $relatedShows = $this->showRepository->createShows($shows);
            $article->shows()->sync($relatedShows);
        }

        if (is_countable($categories) && count($categories) > 0) {
            $relatedCategories = $this->categoryRepository->createCategories($categories);
            $article->categories()->sync($relatedCategories);
        }

        if (is_countable($types) && count($types) > 0) {
            $relatedTypes = $this->typeRepository->createTypes($types);
            $article->types()->sync($relatedTypes);
        }

        if (is_countable($tags) && count($tags) > 0) {
            $relatedTags = $this->tagRepository->createTags($tags);
            $article->tags()->sync($relatedTags);
        }

        return redirect()->action(
            'Admin\ArticleController@show',
            ['article' => $article]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article): View
    {
        $view = view('admin.article.show');
        $view->article = $article;

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article): View
    {
        $view = view('admin.article.edit');
        $view->article = $article;
        $view->slimselect['movies'] = $this->movieRepository->getSlimSelect($article->movies);
        $view->slimselect['shows'] = $this->showRepository->getSlimSelect($article->shows);
        $view->slimselect['categories'] = $this->categoryRepository->getSlimSelect($article->categories);
        $view->slimselect['types'] = $this->typeRepository->getSlimSelect($article->types);
        $view->slimselect['tags'] = $this->tagRepository->getSlimSelect($article->tags);

        return $view;
    }

    public function getMovies(Article $article): array
    {
        return $this->getRelatedItemsArray($article, 'movies');
    }

    public function getShows(Article $article): array
    {
        return $this->getRelatedItemsArray($article, 'shows');
    }

    public function getTypes(Article $article): array
    {
        return $this->getRelatedItemsArray($article, 'types');
    }

    public function getCategories(Article $article): array
    {
        return $this->getRelatedItemsArray($article, 'categories');
    }

    public function getTags(Article $article): array
    {
        return $this->getRelatedItemsArray($article, 'tags');
    }

    private function getRelatedItemsArray(Article $article, string $relation = null): array
    {
        $results = [];

        if ($relation !== null && is_countable($article->{$relation})) {
            foreach ($article->{$relation} as $r) {
                $results[] = [
                    'id' => $r->title,
                    'title' => $r->title,
                ];
            }
        }

        return $results;
    }
}
