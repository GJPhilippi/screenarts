<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovieRequest;
use App\Http\Requests\UpdateMovieRequest;
use App\Movie;
use App\Repositories\MovieRepository;
use App\Services\TraktApiService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

final class MovieController extends Controller
{
    public function __construct()
    {
        $this->movieRepository = new MovieRepository();
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $view = view('admin.movie.index');
        $view->movies = Movie::latest()->take(50)->get();

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.movie.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMovieRequest $request): RedirectResponse
    {
        $traktMovieId = $request->get('trakt-movie-id');

        $movie = $this->movieRepository->createMovie($traktMovieId);

        return redirect()->action(
            'Admin\MovieController@show',
            ['movie' => $movie]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Movie $movie): View
    {
        $view = view('admin.movie.show');
        $view->movie = $movie;

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Movie $movie): View
    {
        $view = view('admin.show.edit');
        $view->movie = $movie;

        return $view;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMovieRequest $request, Movie $movie): RedirectResponse
    {
        $movie->title = $request->get('title');
        $movie->tagline = $request->get('tagline');
        $movie->released_at = $request->get('released_at');
        $movie->status = $request->get('status');
        $movie->country = mb_strtoupper($request->get('country'));
        $movie->runtime = $request->get('runtime');
        $movie->trailer = $request->get('trailer');
        $movie->homepage = $request->get('homepage');
        $movie->overview_nl = $request->get('overview');
        $movie->save();

        return redirect()->action(
            'Admin\MovieController@show',
            ['movie' => $movie]
        );
    }

    /**
     * Search a movie using an API
     */
    public function search(Request $request): array
    {
        $search = $request->query('s');
        $result = TraktApiService::searchMovieForArticle($search);

        $results = [];

        foreach ($result as $movie) {
            $results[] = $movie->movie;
        }

        return $results;
    }
}
