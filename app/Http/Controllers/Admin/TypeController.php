<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTypeRequest;
use App\Http\Requests\UpdateTypeRequest;
use App\Type;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

final class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $view = view('admin.type.index');
        $view->types = Type::latest()->take(50)->get();

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.type.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTypeRequest $request): RedirectResponse
    {
        $title = $request->get('title');

        $type = Type::create([
            'title' => $title,
            'is_active' => 1,
        ]);

        return redirect()->action(
            'Admin\TypeController@show',
            ['type' => $type]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Type $type): View
    {
        $view = view('admin.type.show');
        $view->type = $type;

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Type $type): View
    {
        $view = view('admin.type.edit');
        $view->type = $type;

        return $view;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTypeRequest $request, Type $type): RedirectResponse
    {
        $active = $request->get('is_active') !== null && $request->get('is_active') === 1 ? true : false;

        $type->title = $request->get('title');
        $type->is_active = $active;

        $type->save();

        return redirect()->action(
            'Admin\TypeController@show',
            ['type' => $type]
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Type $type): RedirectResponse
    {
        $type->delete();

        return redirect()->action(
            'Admin\TypeController@index',
        );
    }

    /**
     * @return array[]
     */
    public function search(Request $request): array
    {
        $search = $request->query('s');

        $types = Type::where(['title' => $search, 'is_active' => true])->take(10)->get();

        $result = [];

        foreach ($types as $type) {
            $result[] = [
                'title' => $type->title,
            ];
        }

        return ['data' => $result];
    }
}
