<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Genre;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreShowRequest;
use App\Http\Requests\UpdateShowRequest;
use App\Services\TmdbApiService;
use App\Services\TraktApiService;
use App\Show;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

final class ShowController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $view = view('admin.show.index');
        $view->shows = Show::latest()->take(50)->get();

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.show.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreShowRequest $request): RedirectResponse
    {
        $traktId = $request->get('trakt-show-id');
        $traktInfo = TraktApiService::getShowByTraktId($traktId);
        $trailer = $traktInfo->trailer;
        $homepage = $traktInfo->homepage;

        if (isset($traktInfo->ids->tmdb)) {
            $tmdbInfo = TmdbApiService::getShowByTmdbId($traktInfo->ids->tmdb);

            if (!isset($trailer) && isset($tmdbInfo->trailer)) {
                $trailer = $tmdbInfo->trailer;
            }

            if (!isset($homepage) && isset($tmdbInfo->homepage)) {
                $homepage = $tmdbInfo->homepage;
            }
        }

        $show = new Show();
        $show->title = $traktInfo->title;
        $show->trailer = $trailer;
        $show->homepage = $homepage;
        $show->network = $traktInfo->network;
        $show->country = $traktInfo->country;
        $show->episode_runtime = $traktInfo->runtime;
        $show->airs = $traktInfo->airs;
        $show->year = $traktInfo->year;
        $show->id_trakt = $traktInfo->ids->trakt;
        $show->id_tvdb = $traktInfo->ids->tvdb;
        $show->id_tmdb = $traktInfo->ids->tmdb;
        $show->id_imdb = $traktInfo->ids->imdb;
        $show->id_tvrage = $traktInfo->ids->tvrage;
        $show->status = $traktInfo->status;
        $show->first_aired = Carbon::parse($traktInfo->first_aired);
        $show->overview_en = $traktInfo->overview;
        $show->overview_nl = isset($tmdbInfo->overview) ? $tmdbInfo->overview : null;
        $show->save();

        Genre::setGenresForShow($show, $tmdbInfo->genres);

        return redirect()->action(
            'Admin\ShowController@show',
            ['show' => $show]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Show $show): View
    {
        $view = view('admin.show.show');
        $view->show = $show;

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Show $show): View
    {
        $view = view('admin.show.edit');
        $view->show = $show;

        return $view;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateShowRequest $request, Show $show): RedirectResponse
    {
        $show->title = $request->get('title');
        $show->trailer = $request->get('trailer');
        $show->network = $request->get('network');
        $show->homepage = $request->get('homepage');
        $show->country = mb_strtoupper($request->get('country'));
        $show->episode_runtime = $request->get('episode_runtime');
        $show->year = $request->get('year');
        $show->status = $request->get('status');
        $show->first_aired = $request->get('first_aired');
        $show->overview_nl = $request->get('overview_en');
        $show->overview_nl = $request->get('overview_nl');
        $show->save();

        return redirect()->action(
            'Admin\ShowController@show',
            ['show' => $show]
        );
    }

    /**
     * Search a show using an API
     */
    public function search(Request $request): TraktApiService
    {
        $search = $request->query('s');

        return TraktApiService::searchShowForArticle($search);
    }
}
