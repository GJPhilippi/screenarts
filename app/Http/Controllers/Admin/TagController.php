<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;

final class TagController extends Controller
{
    /**
     * @return array[]
     */
    public function search(Request $request): array
    {
        $search = $request->query('s');

        $tags = Tag::where(['title' => $search, 'is_active' => true])->take(10)->get();

        $result = [];

        foreach ($tags as $tag) {
            $result[] = [
                'id' => $tag->id,
                'title' => $tag->title,
            ];
        }

        return ['data' => $result];
    }
}
