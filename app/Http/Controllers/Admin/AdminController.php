<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

final class AdminController extends Controller
{
    public function index(): View
    {
        return view('admin.index.index');
    }
}
