<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\RedirectResponse as RedirectResponseAlias;
use Illuminate\Http\Request;
use Illuminate\View\View;

final class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $view = view('admin.category.index');
        $view->categories = Category::latest()->take(50)->get();

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request): RedirectResponseAlias
    {
        $title = $request->get('title');

        $category = Category::create([
            'title' => $title,
            'is_active' => 1,
        ]);

        return redirect()->action(
            'Admin\CategoryController@show',
            ['category' => $category]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category): View
    {
        $view = view('admin.category.show');
        $view->category = $category;

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category): View
    {
        $view = view('admin.category.edit');
        $view->category = $category;

        return $view;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category): RedirectResponseAlias
    {
        $active = $request->get('is_active') !== null && $request->get('is_active') === 1;

        $category->title = $request->get('title');
        $category->is_active = $active;

        $category->save();

        return redirect()->action(
            'Admin\CategoryController@show',
            ['category' => $category]
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category): RedirectResponseAlias
    {
        $category->is_active = false;
        $category->save();

        $category->delete();

        return redirect()->action('Admin\CategoryController@index');
    }

    /**
     * @return array[]
     */
    public function search(Request $request): array
    {
        $search = $request->query('s');

        $categories = Category::where(['title' => $search, 'is_active' => true])->take(10)->get();

        $result = [];

        foreach ($categories as $category) {
            $result[] = [
                'id' => $category->id,
                'title' => $category->title,
            ];
        }

        return [
            'data' => $result,
        ];
    }
}
