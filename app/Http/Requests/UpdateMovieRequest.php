<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Country;
use App\Movie;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

final class UpdateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'tagline' => 'string|max:255',
            'released_at' => 'required|date',
            'status' => [
                'required',
                Rule::in(Movie::STATUSES),
            ],
            'country' => [
                'required',
                'max:2',
                Rule::in(array_keys(Country::COUNTIES)),
            ],
            'runtime' => 'required|integer|digits_between:1,3|max:240',
            'trailer' => 'active_url|string',
            'homepage' => 'active_url|string',
            'genres' => 'required',
            'overview' => 'string',
        ];
    }
}
