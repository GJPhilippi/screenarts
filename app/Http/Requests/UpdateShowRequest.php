<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Country;
use App\Show;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

final class UpdateShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'trailer' => 'active_url|string|max:254',
            'homepage' => 'active_url|string|max:254',
            'network' => 'required|string|max:50',
            'status' => [
                'required',
                'string',
                Rule::in(Show::STATUSES),
            ],
            'country' => [
                'required',
                'max:2',
                Rule::in(array_keys(Country::COUNTRIES)),
            ],
            'episode_runtime' => 'integer:max:100',
            'year' => 'integer|min:1900:max:2999',
            'first_aired' => 'required|date',
            'overview_en' => 'string',
            'overview_nl' => 'string',
        ];
    }
}
