<?php

declare(strict_types=1);

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

final class MovieTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * A Fractal transformer.
     *
     * @param $movie
     */
    public function transform($movie): array
    {
        return [
            'title' => $movie->movie->title,
            'id' => $movie->movie->ids->trakt,
            'ids' => $movie->movie->ids,
            'tagline' => $movie->movie->tagline,
            'overview' => $movie->movie->overview,
            'released' => $movie->movie->released,
            'year' => $movie->movie->year,
            'runtime' => $movie->movie->runtime,
            'country' => $movie->movie->country,
            'trailer' => $movie->movie->trailer,
            'homepage' => $movie->movie->homepage,
            'status' => $movie->movie->status,
            'genres' => $movie->movie->genres,
        ];
    }
}
