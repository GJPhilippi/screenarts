<?php

declare(strict_types=1);

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use StdClass;

final class ShowTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * A Fractal transformer.
     */
    public function transform(StdClass $show): array
    {
        return [
            'title' => $show->show->title,
            'id' => $show->show->ids->trakt,
            'ids' => $show->show->ids,
            'overview' => $show->show->overview,
            'year' => $show->show->year,
            'runtime' => $show->show->runtime,
            'country' => $show->show->country,
            'trailer' => $show->show->trailer,
            'homepage' => $show->show->homepage,
            'status' => $show->show->status,
            'genres' => $show->show->genres,
            'airs' => $show->show->airs,
            'network' => $show->show->network,
        ];
    }
}
