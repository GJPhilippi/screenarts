<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Show extends Model
{
    use SoftDeletes;

    public const STATUSES = [
        'returning series',
        'in production',
        'planned',
        'canceled',
        'ended',
    ];

    public $timestamps = true;

    protected $dates = [
        'first_aired',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'is_processed' => 'bool',
        'airs' => 'array',
    ];

    protected $fillable = [
        'id_trakt',
        'title',
    ];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, 'show_genres');
    }
}
