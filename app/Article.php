<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Article extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title',
        'content',
        'slug',
        'published_at',
        'published',
    ];

    protected $casts = [
        'published' => 'boolean',
    ];

    public function movies(): BelongsToMany
    {
        return $this->belongsToMany(Movie::class, 'article_movies');
    }

    public function shows(): BelongsToMany
    {
        return $this->belongsToMany(Show::class, 'article_shows');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'article_categories');
    }

    public function types(): BelongsToMany
    {
        return $this->belongsToMany(Type::class, 'article_types');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'article_tags');
    }
}
