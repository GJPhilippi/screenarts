<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Movie extends Model
{
    use SoftDeletes;

    public const STATUSES = [
        'released',
        'in production',
        'post production',
        'planned',
        'rumored',
        'canceled',
    ];

    public $timestamps = true;

    protected $dates = [
        'released_at',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'is_processed' => 'bool',
    ];

    protected $fillable = [
        'id_trakt',
        'title',
    ];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, 'movie_genres');
    }

    public function articles(): BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'article_movies');
    }
}
