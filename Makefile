.PHONY: all

info: intro usage

define HEADER
       _______.  ______ .______       _______  _______ .__   __.      ___      .______     .___________.    _______.     ______   .__   __.  __       __  .__   __.  _______
      /       | /      ||   _  \     |   ____||   ____||  \ |  |     /   \     |   _  \    |           |   /       |    /  __  \  |  \ |  | |  |     |  | |  \ |  | |   ____|
     |   (----`|  ,----'|  |_)  |    |  |__   |  |__   |   \|  |    /  ^  \    |  |_)  |   `---|  |----`  |   (----`   |  |  |  | |   \|  | |  |     |  | |   \|  | |  |__
      \   \    |  |     |      /     |   __|  |   __|  |  . `  |   /  /_\  \   |      /        |  |        \   \       |  |  |  | |  . `  | |  |     |  | |  . `  | |   __|
  .----)   |   |  `----.|  |\  \----.|  |____ |  |____ |  |\   |  /  _____  \  |  |\  \----.   |  |    .----)   |      |  `--'  | |  |\   | |  `----.|  | |  |\   | |  |____
  |_______/     \______|| _| `._____||_______||_______||__| \__| /__/     \__\ | _| `._____|   |__|    |_______/        \______/  |__| \__| |_______||__| |__| \__| |_______|
endef
export HEADER

intro:
	@echo "$$HEADER"

usage:
	@echo "Project:"
	@echo "  make init                        Initialise the project for development."
	@echo "  make serve                       Run the project for development."
	@echo "\nEnvironment:"
	@echo "  make assets                      Compile the assets for development."
	@echo "  make watch                       Start the file watchers for development."
	@echo "  make db-rebuild                  Force rebuild the database from scratch and run migrations + seeds."
	@echo "\nApplication:"
	@echo "  make tinker                      Start up Tinker for command line interaction with the application."
	@echo "\nTests and checks:"
	@echo "  make tests                       Run the tests."
	@echo "  make codestyle                   Run the codestyle checks."
	@echo "  make codestyle-fix               Run the codestyle checks and fix them automatically."

# ===========================
# Commands
# ===========================

init: intro do_init do_assets
serve: intro do_serve
assets: intro do_assets
watch: intro do_watch
db-rebuild: intro do_db_rebuild
tinker: intro do_tinker
tests: intro do_tests
codestyle: intro do_codestyle
codestyle-fix: intro do_codestyle_fix

# ===========================
# Recipes
# ===========================

do_init:
	composer install
	php -r "file_exists('.env') || copy('.env.example', '.env');"
	php artisan key:generate --ansi
	yarn install --frozen-lockfile

do_serve:
	php artisan serve

do_assets:
	yarn run dev
	php artisan telescope:install

do_watch:
	yarnnpm run watch

do_db_rebuild:
	php artisan migrate:fresh --seed --force

do_tinker:
	php artisan tinker

do_tests:
ifdef filter
	vendor/bin/phpunit --filter=$(filter) $(arguments)
else
	vendor/bin/phpunit $(arguments)
endif

do_codestyle:
	vendor/bin/ecs --config=dev/ecs/config.php check .

do_codestyle_fix:
	vendor/bin/ecs --config=dev/ecs/config.php check --fix .
