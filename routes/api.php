<?php

declare(strict_types=1);
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::middleware('throttle:60,1')->group(function () {
    Route::group(['as' => 'api.admin.', 'name' => 'admin.', 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
        // Custom routes
        Route::get('movie/search', 'MovieController@search')->name('movie.search');
        Route::get('show/search', 'ShowController@search')->name('show.search');
        Route::get('category/search', 'CategoryController@search')->name('category.search');
        Route::get('type/search', 'TypeController@search')->name('type.search');
        Route::get('tag/search', 'TagController@search')->name('tag.search');
        Route::get('article/get-movies/{article}', 'ArticleController@getMovies')->name('article.get-movies');
        Route::get('article/get-shows/{article}', 'ArticleController@getShows')->name('article.get-shows');
        Route::get('article/get-types/{article}', 'ArticleController@getTypes')->name('article.get-types');
        Route::get('article/get-categories/{article}', 'ArticleController@getCategories')->name('article.get-categories');
        Route::get('article/get-tags/{article}', 'ArticleController@getTags')->name('article.get-tagss');
    });
});
