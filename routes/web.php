<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'admin.', 'name' => 'admin.', 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::resource('article', 'ArticleController');
    Route::resource('movie', 'MovieController');
    Route::resource('show', 'ShowController');
    Route::resource('type', 'TypeController');
    Route::resource('tag', 'TagController');
    Route::resource('category', 'CategoryController');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
