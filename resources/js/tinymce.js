document.addEventListener('DOMContentLoaded', (event) => {
    tinymce.init({
        selector: '#articleContent',
        plugins: [
            'autoresize advlist autolink link image lists charmap preview hr anchor pagebreak spellchecker',
            'searchreplace visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'table emoticons paste help'
        ],
        toolbar: 'undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify |' +
            ' bullist numlist outdent indent | link image media | print preview fullpage | ' +
            'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help',
        base_url: '/js/tinymce/',
        lang: 'nl',
        spellchecker_language: 'nl',
        width: '100%',
    });
})



