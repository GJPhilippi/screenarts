<!DOCTYPE HTML>
<html>
<head>
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="/vendor/backend/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/animation/css/animate.min.css">

    <!-- notification css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/notification/css/notification.min.css">

    <!-- vendor css -->
    <link rel="stylesheet" href="/vendor/backend/css/style.css">

    <title>ScreenArts admin</title>
</head>
    <body>
        <form method="post" action="/login">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="auth-wrapper">
                <div class="auth-content">
                    <div class="card">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <h4 class="mb-3 f-w-400">Inloggen op de beheeromgeving</h4>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Emailadres">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control" placeholder="Wachtwoord">
                                    </div>
                                    <div class="text-left">
                                        <div class="checkbox checkbox-primary d-inline">
                                            <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" checked="">
                                            <label for="checkbox-fill-a1" class="cr">Ingelogd blijven</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Inloggen</button>
                                    <p class="text-muted">Wachtwoord vergeten? <a href="{{ route('password.request') }}" class="f-w-400">Resetten</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
