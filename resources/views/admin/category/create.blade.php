@extends('admin.base')

@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Toevoegen categorie</h5>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => ['admin.category.store'], 'method' => 'POST', 'id' => 'sa-new-article']) !!}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::text('title', null, ['id' => 'articleTitle', 'class' => 'form-control', 'placeholder' => 'Naam van de categorie']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <!-- multi-select css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/multi-select/css/multi-select.css">
    <link rel="stylesheet" href="/vendor/backend/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">
    <!-- Bootstrap datetimepicker css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="/vendor/backend/fonts/material/css/materialdesignicons.min.css">
    <!-- minicolors css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/mini-color/css/jquery.minicolors.css">
    <!-- multi-select css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/multi-select/css/multi-select.css">

    <!-- bootstrap-tagsinput-latest css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-tagsinput-latest/css/bootstrap-tagsinput.css">

    <link rel="stylesheet" href="/vendor/backend/css/style.css">
@endsection

@section('extra-js')
    <script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="/vendor/backend/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- minicolors Js -->
    <script src="/vendor/backend/plugins/mini-color/js/jquery.minicolors.min.js"></script>

    <!-- multi-select Js -->
    <script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- TinyMCE -->
    <script src="/js/tinymce/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/js/tinymce/langs/nl.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: '#articleContent',
            plugins: [
                'autoresize advlist autolink link image lists charmap preview hr anchor pagebreak spellchecker',
                'searchreplace visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'table emoticons paste help'
            ],
            toolbar: 'undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify |' +
                ' bullist numlist outdent indent | link image media | print preview fullpage | ' +
                'forecolor backcolor emoticons | help',
            menubar: 'file edit view insert format tools table help',
            content_css: 'css/content.css',
            lang: 'nl',
            spellchecker_language: 'nl',
            width: '100%',
        });
    </script>

    <script type="text/javascript">
        $('.date-format').bootstrapMaterialDatePicker({
            format: 'Y-M-D HH:mm',
            minDate: new Date(),
            lang: 'nl'
        });
    </script>

    <script src="/js/backend/article/select2.category.js"></script>
    <script src="/js/backend/article/select2.movies.js"></script>
    <script src="/js/backend/article/select2.shows.js"></script>
    <script src="/js/backend/article/select2.types.js"></script>
    <script src="/js/backend/article/select2.tags.js"></script>

@endsection
