@extends('admin.base')
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h5>Categories</h5>
                <hr>
                <table id="footable-table" class="table table-striped table-hover footable footable-1 footable-paging footable-paging-center breakpoint-lg" style="">
                    <thead>
                    <tr class="footable-header">
                        <th class="footable-sortable footable-first-visible" >
                            Categorie
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-sortable footable-first-visible" >
                            Actief?
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-sortable footable-first-visible" >
                            # Artikelen
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Aangemaakt op
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-last-visible">
                            Opties
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td><a href="{{ route('admin.category.show', $category) }}">{{ $category->title }}</a></td>
                                <td>@if ($category->is_active) Ja @else Nee @endif </td>
                                <td>
                                    {{ $category->articles->count() }}
                                </td>
                                <td>
                                    {{ $category->created_at->format('d-m-Y H:i') }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.category.edit', $category) }}" class="btn btn-primary">Bewerken</a>
                                    <a href="{{ route('admin.category.destroy', $category->id) }}" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-category="{{ $category->title }}">Verwijder</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Verwijderen categorie</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                Weet je zeker dat je deze categorie wilt verwijderen?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary">Annuleren</button>
                                {{ Form::open(['method' => 'DELETE', 'id' => 'deleteCategoryForm']) }}
                                    <button type="submit" class="btn btn-danger">Verwijderen</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <!-- datatables css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/data-tables/css/datatables.min.css">

    <!-- footable css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.standalone.min.css">
@endsection

@section('extra-js')
    <script src="/vendor/backend/plugins/footable/js/footable.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#footable-table').footable({
                "paging": {
                    "enabled": false,
                    "size": 50
                },
                "sorting": {
                    "enabled": true
                }
            });
        });
    </script>
    <script>
        $('#deleteModal').on('show.bs.modal', function(e) {
            let button = $(e.relatedTarget);
            let category = button.data('category');
            let modal = $(this);

            modal.find('.modal-title').text('Verwijderen categorie ' + category);
            modal.find('.modal-body').text('Weet je zeker dat je de categorie ' + category + ' wilt verwijderen?');
            modal.find('#deleteCategoryForm').attr('action', button.attr('href'));
        })
    </script>
@endsection
