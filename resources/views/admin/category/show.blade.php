@extends('admin.base')
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h5>Bekijk categorie "{{$category->title}}"</h5>
                <hr>
                <h6>Artikelen bij deze categorie</h6>
                <table id="footable-table" class="table table-striped table-hover footable footable-1 footable-paging footable-paging-center breakpoint-lg" style="">
                    <thead>
                    <tr class="footable-header">
                        <th class="footable-sortable footable-first-visible" >
                            Titel
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Gepubliceerd (op)
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Type
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-last-visible">
                            Opties
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($category->articles as $article)
                        <tr>
                            <td><a href="{{ route('admin.article.show', $article) }}">{{ $article->title }}</a></td>
                            <td>
                                @if ($article->published)
                                    Ja
                                @else
                                    Nee
                                @endif
                                ({{ $article->published_at->format('Y-m-d H:i') }})</td>
                            <td>
                                <ul>
                                @foreach($article->types as $type)
                                    <li>{{ $type->type }}</li>
                                @endforeach
                                </ul>
                            </td>
                            <td>
                                <a href="{{ route('admin.article.show', $article) }}">Bekijken</a>
                                <a href="{{ route('admin.article.edit', $article) }}">Bewerken</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
