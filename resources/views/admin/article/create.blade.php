@extends('admin.base')

@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Toevoegen artikel</h5>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => ['admin.article.store'], 'method' => 'POST', 'id' => 'sa-new-article']) !!}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::text('title', null, ['id' => 'articleTitle', 'class' => 'form-control', 'placeholder' => 'Naam van het artikel']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::select('movies[]', [], null, ['id' => 'articleMovies', 'class' => 'form-control', 'multiple', 'tabindex' => '-1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::select('shows[]', [], null, ['id' => 'articleShows', 'class' => 'form-control', 'multiple' => 'multiple', 'tabindex' => '-1', 'aria-hidden']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::select('types[]', [], null, ['id' => 'articleTypes', 'class' => 'form-control', 'multiple' => 'multiple', 'tabindex' => '-1', 'aria-hidden']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::select('categories[]', [], null, ['id' => 'articleCategories', 'class' => 'form-control', 'multiple' => 'multiple', 'tabindex' => '-1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::select('tags[]', [], null, ['id' => 'articleTags', 'class' => 'form-control', 'multiple' => 'multiple', 'tabindex' => '-1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    Gepubliceerd? Nee
                                    <div class="switch switch-success d-inline m-r-10">
                                        <input name="published" value="1" type="checkbox" id="switch-s-1"s>
                                        <label for="switch-s-1" class="cr"></label>
                                    </div>
                                    Ja
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::text('published_at', null, ['id' => 'articlePublishDate', 'class' => 'form-control date-format', 'placeholder' => 'Publiceerdatum van het artikel', 'readonly' => 'readonly']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::text('image', null, ['id' => 'articlePicture', 'class' => 'form-control', 'placeholder' => 'Uitgelichte afbeelding van het artikel']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-group row">
                                <!-- [ tinymce editor ] start -->
                                {!! Form::textarea('content', null, ['id' => 'articleContent', 'class' => 'form-control tinymce-editor', 'placeholder' => 'Inhoud van het artikel']) !!}
                                <!-- [ tinymce editor ] end -->
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <!-- select2 css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/select2/css/select2.min.css">
    <!-- multi-select css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/multi-select/css/multi-select.css">
    <link rel="stylesheet" href="/vendor/backend/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">
    <!-- Bootstrap datetimepicker css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="/vendor/backend/fonts/material/css/materialdesignicons.min.css">
    <!-- minicolors css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/mini-color/css/jquery.minicolors.css">

    <!-- Slim Select CSS -->
    <link rel="stylesheet" href="/vendor/backend/plugins/slim-select/dist/slimselect.min.css">

    <!-- bootstrap-tagsinput-latest css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-tagsinput-latest/css/bootstrap-tagsinput.css">

    <link rel="stylesheet" href="/vendor/backend/css/style.css">
@endsection

@section('extra-js')
    <script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="/vendor/backend/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- minicolors Js -->
    <script src="/vendor/backend/plugins/mini-color/js/jquery.minicolors.min.js"></script>

    <!-- Slim Select -->
    <script src="/vendor/backend/plugins/slim-select/dist/slimselect.min.js"></script>

    <!-- multi-select Js -->
    <script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- TinyMCE -->
    <script src="/js/tinymce/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="/js/tinymce/langs/nl.js" referrerpolicy="origin"></script>

    <!-- Slim select JS -->
    <script src="/js/backend/article/create.js" type="text/javascript"></script>

    <script>
        tinymce.init({
            selector: '#articleContent',
            plugins: [
                'autoresize advlist autolink link image lists charmap preview hr anchor pagebreak spellchecker',
                'searchreplace visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'table emoticons paste help'
            ],
            toolbar: 'undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify |' +
                ' bullist numlist outdent indent | link image media | print preview fullpage | ' +
                'forecolor backcolor emoticons | help',
            menubar: 'file edit view insert format tools table help',
            content_css: '/js/tinymce/skins/content/default/content.min.css',
            lang: 'nl',
            spellchecker_language: 'nl',
            width: '100%',
        });
    </script>

    <script type="text/javascript">
        $('.date-format').bootstrapMaterialDatePicker({
            format: 'Y-M-D HH:mm',
            minDate: new Date(),
            lang: 'nl'
        });
    </script>
@endsection
