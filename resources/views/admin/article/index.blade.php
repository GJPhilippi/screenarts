@extends('admin.base')
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h5>Artikelen</h5>
                <hr>
                <table id="footable-table" class="table table-striped table-hover footable footable-1 footable-paging footable-paging-center breakpoint-lg" style="">
                    <thead>
                    <tr class="footable-header">
                        <th class="footable-sortable footable-first-visible" >
                            Titel
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-sortable footable-first-visible" >
                            Auteur
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Gepubliceerd (op)
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Type
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Categorien
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-last-visible">
                            Opties
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td><a href="{{ route('admin.article.show', $article) }}">{{ $article->title }}</a></td>
                                <td>{{ $article->author->name ?? '' }}</td>
                                <td>
                                    @if ($article->published)
                                        Ja
                                    @else
                                        Nee
                                    @endif
                                    ({{ $article->published_at->format('Y-m-d H:i') }})
                                </td>
                                <td>
                                    <ul>
                                        @foreach($article->types as $type)
                                            <li>{{$type->type }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                        @foreach($article->categories as $category)
                                            <li>{{$category->category }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <!-- datatables css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/data-tables/css/datatables.min.css">

    <!-- footable css -->
    <link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.standalone.min.css">
@endsection

@section('extra-js')
    <script src="/vendor/backend/plugins/footable/js/footable.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#footable-table').footable({
                "paging": {
                    "enabled": false,
                    "size": 50
                },
                "sorting": {
                    "enabled": true
                }
            });
        });
    </script>
@endsection
