@extends('admin.base')
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h5>{{ $article->title }}</h5>
                <hr>
                {!! Purifier::clean($article->content, 'youtube') !!}
            </div>
        </div>
    </div>
@endsection
