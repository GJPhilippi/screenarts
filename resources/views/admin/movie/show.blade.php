@extends('admin.base')
@section('content')
<div class="col-xl-4 col-md-6">
    <div class="card user-card user-card-1">
        <div class="card-header border-0 p-2 pb-0">
            <div class="cover-img-block">
                <img src="/vendor/backend/images/widget/slider7.jpg" alt="" class="img-fluid">
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="user-about-block text-center">
                <div class="row align-items-end">
                    <div class="col text-left pb-3"></div>
                    <div class="col"><img class="img-radius img-fluid wid-80" src="/vendor/backend/images/user/avatar-1.jpg" alt="User image"></div>
                    <div class="col text-right pb-3">
                        <div class="dropdown">
                            <a class="drp-icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-more-horizontal"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ route('admin.movie.edit', $movie) }}">Bewerken</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <h6 class="mb-1 mt-3">{{ $movie->title }}</h6>
                <p class="mb-3 text-muted">{{ $movie->tagline }}</p>
                <h6 class="mb-1 mt-3">Overview English</h6>
                <p class="mb-1 mt-3">{{ $movie->overview_en }}</p>
                @if (isset($movie->overview_nl))
                    <h6 class="mb-1 mt-3">Overview Dutch</h6>
                    <p class="mb-0">{{ $movie->overview_nl }}</p>
                @endif
                <h6 class="mb-1 mt-3">Genres</h6>
                <p class="mb-3 text-muted">
                    <ul>
                        @foreach($movie->genres as $genre)
                            <li>{{ $genre->genre }}</li>
                        @endforeach
                    </ul>
                </p>
            </div>
            <hr class="wid-80 b-wid-3 my-4">
            <div class="row text-center">
                <div class="col">
                    <h6 class="mb-1"><a href="{{ $movie->homepage }}" rel="nofollow" target="_blank">Website</a></h6>
                    <p class="mb-0">Homepage</p>
                </div>
                <div class="col">
                    <h6 class="mb-1">2749</h6>
                    <p class="mb-0">Artikelen</p>
                </div>
                <div class="col">
                    <h6 class="mb-1"><a href="{{ $movie->trailer }}" rel="nofollow" target="_blank">YouTube</a></h6>
                    <p class="mb-0">Trailer</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
