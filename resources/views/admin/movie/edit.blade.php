@extends('admin.base')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Film bewerken</h5>
        </div>
        <div class="card-body">
            <h5>{{ $movie->title }}</h5>
            <div class="row">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{ Form::open(['action' => ['Admin\MovieController@update', $movie->id], 'method' => 'PATCH']) }}
                        <div class="form-group row">
                            <label for="movieTitle" class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                {!! Form::text('title', $movie->title, ['id' => 'movieTitle', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieTagline" class="col-sm-3 col-form-label">Tagline</label>
                            <div class="col-sm-9">
                                {!! Form::text('tagline', $movie->tagline, ['id' => 'movieTagline', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieRelease" class="col-sm-3 col-form-label">Released (Y-m-d)</label>
                            <div class="col-sm-9">
                                {!! Form::text('released_at', $movie->released_at->format('Y-m-d'), ['id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieStatus" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                {!! Form::select('status', App\Movie::STATUSES, $movie->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieCountry" class="col-sm-3 col-form-label">Country (production)</label>
                            <div class="col-sm-9">
                                {!! Form::select('country', App\Country::COUNTRIES, strtoupper($movie->country), ['class' => 'form-control js-example-basic-single select2-hidden-accessible', 'data-select2-id' => 1, 'tabindex' => '-1', 'aria-hidden' => 'true']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieRuntime" class="col-sm-3 col-form-label">Runtime (in minutes)</label>
                            <div class="col-sm-9">
                                {!! Form::number('runtime', $movie->runtime, ['id' => 'movieRuntime', 'class' => 'form-control', 'min' => 1, 'max' => 200]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieTrailer" class="col-sm-3 col-form-label">
                                Trailer - <a href="{{ $movie->trailer }}" rel="nofollow" target="_blank">Watch it</a>
                            </label>
                            <div class="col-sm-9">
                                {!! Form::text('trailer', $movie->trailer, ['id' => 'movieTrailer', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieHomepage" class="col-sm-3 col-form-label">Homepage</label>
                            <div class="col-sm-9">
                                {!! Form::text('homepage', $movie->homepage, ['id' => 'movieHomepage', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieGenres" class="col-sm-3 col-form-label">Genres (comma separated)</label>
                            <div class="col-sm-9">
                                {!! Form::text('genres', implode(',', $movie->genres), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieOverviewEn" class="col-sm-3 col-form-label">Overview (English)</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('overview_en', $movie->overview_en, ['id' => 'movieOverviewEn', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="movieOverviewNl" class="col-sm-3 col-form-label">Overview (Dutch)</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('overview_nl', $movie->overview_nl, ['id' => 'movieOverviewNl', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
        <script src="/vendor/backend/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
        <script src="/vendor/backend/plugins/select2/js/i18n/nl.js"></script>
        <script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
        <script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>
        <script src="/vendor/backend/plugins/mini-color/js/jquery.minicolors.min.js"></script>
        <script src="/vendor/backend/js/pages/form-picker-custom.js"></script>
        <script src="/vendor/backend/plugins/bootstrap-tagsinput-latest/js/bootstrap-tagsinput.min.js"></script>
        <script src="/vendor/backend/js/pages/form-select-custom.js"></script>
@endsection

@section('extra-css')
<!-- material datetimepicker css -->
        <link rel="stylesheet" href="/vendor/backend/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">
        <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="/vendor/backend/fonts/material/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="/vendor/backend/plugins/mini-color/css/jquery.minicolors.css">

        <link rel="stylesheet" href="/vendor/backend/plugins/select2/css/select2.min.css">
@endsection
