@extends('admin.base')
@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <h5>Movies</h5>
            <hr>
            <table id="footable-table" class="table table-striped table-hover footable footable-1 footable-paging footable-paging-center breakpoint-lg" style="">
                <thead>
                    <tr class="footable-header">
                        <th class="footable-sortable footable-first-visible" >
                            Titel
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-sortable footable-first-visible" >
                            Ondertitel
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Jaar
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Speelduur
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Status
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th data-breakpoints="xs" class="footable-sortable" >
                            Verwerkt?
                            <span class="fooicon fooicon-sort"></span>
                        </th>
                        <th class="footable-sortable footable-last-visible">
                            Datums
                        </th>
                        <th class="footable-last-visible">
                            Opties
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($movies as $movie)
                    <tr>
                        <td>
                            <a href="{{ route('admin.movie.show', $movie) }}">{{ $movie->title }}</a>
                        </td>
                        <td>{{ $movie->tagline }}</td>
                        <td>{{ $movie->released_at->year }}</td>
                        <td>{{ $movie->runtime }}m</td>
                        <td>{{ $movie->status }}</td>
                        <td>
                            @if ($movie->is_processed)
                                <i class="feather icon-check-square"></i>
                            @else
                                <i class="feather icon-square"></i>
                            @endif
                        </td>
                        <td>
                            Added: {{ $movie->created_at->format('d-m-Y H:i:s') }}
                            @if ($movie->created_at != $movie->updated_at)
                                <br>Updated: {{ $movie->updated_at->format('d-m-Y H:i:s') }}
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin.movie.edit', $movie) }}">Bewerken</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('extra-css')
<!-- datatables css -->
<link rel="stylesheet" href="/vendor/backend/plugins/data-tables/css/datatables.min.css">

<!-- footable css -->
<link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="/vendor/backend/plugins/footable/css/footable.standalone.min.css">
@endsection

@section('extra-js')
<script src="/vendor/backend/plugins/footable/js/footable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#footable-table').footable({
            "paging": {
                "enabled": false,
                "size": 50
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>
@endsection
