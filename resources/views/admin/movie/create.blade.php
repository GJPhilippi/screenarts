@extends('admin.base')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Toevoegen movie</h5>
        </div>
        <div class="card-body">
            {{ Form::open(['route' => ['admin.movie.store'], 'method' => 'POST', 'id' => 'sa-new-movie']) }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <select name="trakt-movie-id" id="movie-name" class="col-sm-12 select2-hidden-accessible" tabindex="-1" aria-hidden="true"></select>
                    </div>
                </div>
                {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('extra-css')
<!-- select2 css -->
<link rel="stylesheet" href="/vendor/backend/plugins/select2/css/select2.min.css">
<!-- multi-select css -->
<link rel="stylesheet" href="/vendor/backend/plugins/multi-select/css/multi-select.css">
@endsection

@section('extra-js')
<!-- select2 Js -->
<script src="/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="/vendor/backend/plugins/select2/js/i18n/nl.js"></script>

<!-- multi-select Js -->
<script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
<script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>

<script type="text/javascript">
    $("#movie-name").select2({
        ajax: {
            method: "GET",
            url: "{{ route('api.admin.movie.search') }}",
            //dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    s: params.term, // search term
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Naam van de movie',
        minimumInputLength: 1,
        lang: 'nl',
        templateResult: formatMovie,
        templateSelection: formatMovieSelection
    });

    function formatMovie (movie) {
        if (!movie.year) {
            movie.year = 'onbekend';
        }

        var $container = $(
            '<div class="select2-result-movie clearfix">' +
                '<div class="select2-result-movie__title">' + movie.title + ' (' + movie.year + ')</div>' +
                '<div class="select2-result-movie__meta">' +
                    '<div class="select2-result-movie__tagline">' + movie.tagline + '</div>' +
                '</div>' +
            '</div>'
        );

        return $container;
    }

    function formatMovieSelection (movie) {
        return movie.title || movie.text;
    }
</script>
@endsection
