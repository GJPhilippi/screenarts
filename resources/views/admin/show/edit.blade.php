@extends('admin.base')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Film bewerken</h5>
        </div>
        <div class="card-body">
            <h5>{{ $show->title }}</h5>
            <div class="row">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{ Form::open(['action' => ['Admin\ShowController@update', $show->id], 'method' => 'PATCH']) }}
                        <div class="form-group row">
                            <label for="showTitle" class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                {!! Form::text('title', $show->title, ['id' => 'showTitle', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showTrailer" class="col-sm-3 col-form-label">
                                Trailer - <a href="{{ $show->trailer }}" rel="nofollow" target="_blank">Watch it</a>
                            </label>
                            <div class="col-sm-9">
                                {!! Form::text('trailer', $show->trailer, ['id' => 'showTrailer', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showRelease" class="col-sm-3 col-form-label">Jaar</label>
                            <div class="col-sm-9">
                                {!! Form::text('year', $show->year, ['class' => 'form-control', 'placeholder' => $show->year]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showRelease" class="col-sm-3 col-form-label">Eerste uitzending (Y-m-d)</label>
                            <div class="col-sm-9">
                                {!! Form::text('first_aired', $show->first_aired->format('Y-m-d'), ['id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showNetwork" class="col-sm-3 col-form-label">Zender</label>
                            <div class="col-sm-9">
                                {!! Form::text('network', $show->network, ['class' => 'form-control', 'placeholder' => $show->network]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showStatus" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                {!! Form::select('status', App\Show::STATUSES, $show->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showCountry" class="col-sm-3 col-form-label">Country (production)</label>
                            <div class="col-sm-9">
                                {!! Form::select('country', App\Country::COUNTRIES, strtoupper($show->country), ['class' => 'form-control js-example-basic-single select2-hidden-accessible', 'data-select2-id' => 1, 'tabindex' => '-1', 'aria-hidden' => 'true']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showRuntime" class="col-sm-3 col-form-label">Runtime (in minutes)</label>
                            <div class="col-sm-9">
                                {!! Form::number('episode_runtime', $show->episode_runtime, ['id' => 'showRuntime', 'class' => 'form-control', 'min' => 1, 'max' => 100]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showHomepage" class="col-sm-3 col-form-label">Homepage</label>
                            <div class="col-sm-9">
                                {!! Form::text('homepage', $show->homepage, ['id' => 'showHomepage', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showGenres" class="col-sm-3 col-form-label">Genres (comma separated)</label>
                            <div class="col-sm-9">

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showOverviewEn" class="col-sm-3 col-form-label">Overview (English)</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('overview_en', $show->overview_en, ['id' => 'showOverviewEn', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="showOverviewNl" class="col-sm-3 col-form-label">Overview (Dutch)</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('overview_nl', $show->overview_nl, ['id' => 'showOverviewNl', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
        <script src="/vendor/backend/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
        <script src="/vendor/backend/plugins/select2/js/i18n/nl.js"></script>
        <script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
        <script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>
        <script src="/vendor/backend/plugins/mini-color/js/jquery.minicolors.min.js"></script>
        <script src="/vendor/backend/js/pages/form-picker-custom.js"></script>
        <script src="/vendor/backend/plugins/bootstrap-tagsinput-latest/js/bootstrap-tagsinput.min.js"></script>
        <script src="/vendor/backend/js/pages/form-select-custom.js"></script>
@endsection

@section('extra-css')
<!-- material datetimepicker css -->
        <link rel="stylesheet" href="/vendor/backend/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">
        <link rel="stylesheet" href="/vendor/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="/vendor/backend/fonts/material/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="/vendor/backend/plugins/mini-color/css/jquery.minicolors.css">

        <link rel="stylesheet" href="/vendor/backend/plugins/select2/css/select2.min.css">
@endsection
