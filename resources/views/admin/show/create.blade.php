@extends('admin.base')

@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Toevoegen show</h5>
        </div>
        <div class="card-body">
            {{ Form::open(['route' => ['admin.show.store'], 'method' => 'POST', 'id' => 'sa-new-serie']) }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <select name="trakt-show-id" id="show-name" class="col-sm-12 select2-hidden-accessible" tabindex="-1" aria-hidden="true"></select>
                    </div>
                </div>
            {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('extra-css')
<!-- select2 css -->
<link rel="stylesheet" href="/vendor/backend/plugins/select2/css/select2.min.css">
<!-- multi-select css -->
<link rel="stylesheet" href="/vendor/backend/plugins/multi-select/css/multi-select.css">
@endsection

@section('extra-js')
<!-- select2 Js -->
<script src="/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="/vendor/backend/plugins/select2/js/i18n/nl.js"></script>

<!-- multi-select Js -->
<script src="/vendor/backend/plugins/multi-select/js/jquery.quicksearch.js"></script>
<script src="/vendor/backend/plugins/multi-select/js/jquery.multi-select.js"></script>

<script type="text/javascript">
    $("#show-name").select2({
        ajax: {
            method: "GET",
            url: "{{ route('api.admin.show.search') }}",
            //dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    s: params.term, // search term
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Naam van de show',
        minimumInputLength: 1,
        templateResult: formatShow,
        templateSelection: formatShowSelection
    });

    function formatShow (show) {
        if (!show.year) {
            show.year = 'onbekend';
        }

        var $container = $(
            '<div class="select2-result-show clearfix">' +
                '<div class="select2-result-show__title">' + show.title + ' (' + show.year + '), ' + show.network + '</div>' +
            '</div>'
        );

        return $container;
    }

    function formatShowSelection (serie) {
        return serie.title || serie.text;
    }
</script>
@endsection
