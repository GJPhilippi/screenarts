<!DOCTYPE HTML>
<html>
    <head>
        <!-- fontawesome icon -->
        <link rel="stylesheet" href="/vendor/backend/fonts/fontawesome/css/fontawesome-all.min.css">
        <!-- animation css -->
        <link rel="stylesheet" href="/vendor/backend/plugins/animation/css/animate.min.css">

        <!-- notification css -->
        <link rel="stylesheet" href="/vendor/backend/plugins/notification/css/notification.min.css">

        <!-- modal styling -->
        <link rel="stylesheet" href="/vendor/backend/plugins/modal-window-effects/css/md-modal.css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="/vendor/backend/css/style.css">

        <!-- Custom added CSS per page -->
        @yield('extra-css')

        <title>ScreenArts admin</title>

        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <body>
        <!-- Navbar on the left -->
        <nav class="pcoded-navbar menupos-fixed">
            <div class="navbar-wrapper ">
                <div class="navbar-brand header-logo">
                    <a href="{{ route('admin.index') }}" class="b-brand">
                        <div class="b-bg">
                            SA
                        </div>
                        <span class="b-title">ScreenArts</span>
                    </a>
                    <a class="mobile-menu" id="mobile-collapse"><span></span></a>
                </div>
                <div class="navbar-content scroll-div">
                    <ul class="nav pcoded-inner-navbar ">
                        <li class="nav-item pcoded-menu-caption">
                            <label>Beheermenu</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-file"></i>
                                </span>
                                <span class="pcoded-mtext">Artikelen</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.article.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.article.create') }}">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-sun"></i>
                                </span>
                                <span class="pcoded-mtext">Categorieën</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.category.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.category.create') }}">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-star"></i>
                                </span>
                                <span class="pcoded-mtext">Typen</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.type.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.type.create') }}">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-tag"></i>
                                </span>
                                <span class="pcoded-mtext">Tags</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.tag.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.tag.create') }}">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-film"></i>
                                </span>
                                <span class="pcoded-mtext">Movies</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.movie.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.movie.create') }}" class="md-trigger" data-modal="modal-add-movie">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li data-username="widget statistic data chart" class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-monitor"></i>
                                </span>
                                <span class="pcoded-mtext">Series</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('admin.show.index') }}">Overzicht</a></li>
                                <li><a href="{{ route('admin.show.create') }}">Toevoegen</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#" class="nav-link">
                                <span class="pcoded-micon">
                                    <i class="feather icon-users"></i>
                                </span>
                                <span class="pcoded-mtext">Gebruikers</span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li><a href="">Analytics</a></li>
                                <li><a href="">Sales</a></li>
                                <li><a href="dashboard-project.html">Project</a></li>
                                <li><a href="dashboard-help.html">Helpdesk<span class="pcoded-badge label label-danger">NEW</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header on top -->
        <header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed header-dark">
            <div class="m-header">
                <a class="mobile-menu" id="mobile-collapse1" href="#"><span></span></a>
                <a href="index.html" class="b-brand">
                    <div class="b-bg">
                        SA
                    </div>
                    <span class="b-title">ScreenArts</span>
                </a>
            </div>
            <a class="mobile-menu" id="mobile-header" href="#">
                <i class="feather icon-more-horizontal"></i>
            </a>
            <div class="collapse navbar-collapse">
                <a href="#" class="mob-toggler"></a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <div class="main-search">
                            <div class="input-group">
                                <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                                <a href="#" class="input-group-append search-close">
                                    <i class="feather icon-x input-group-text"></i>
                                </a>
                                <span class="input-group-append search-btn btn btn-primary">
                                    <i class="feather icon-search input-group-text"></i>
                                </span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown drp-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon feather icon-settings"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-notification">
                                <div class="pro-head">
                                    <img src="/vendor/backend/images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
                                    <span>John Doe</span>
                                    <a href="auth-signin.html" class="dud-logout" title="Logout">
                                        <i class="feather icon-log-out"></i>
                                    </a>
                                </div>
                                <ul class="pro-body">
                                    <li><a href="#" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                    <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
                                    <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </header>

        <section class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <div class="row">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script src="/vendor/backend/js/vendor-all.min.js"></script>
        <script src="/vendor/backend/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/vendor/backend/js/pcoded.min.js"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @yield('extra-js')
    </body>
</html>
