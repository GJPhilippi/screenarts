@extends('admin.base')

@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Toevoegen type</h5>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => ['admin.type.store'], 'method' => 'POST', 'id' => 'sa-new-article']) !!}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::text('title', null, ['id' => 'articleTitle', 'class' => 'form-control', 'placeholder' => 'Naam van het type']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <link rel="stylesheet" href="/vendor/backend/css/style.css">
@endsection

@section('extra-js')

@endsection
