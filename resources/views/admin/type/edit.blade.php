@extends('admin.base')

@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Bewerken categorie</h5><small>{{ $type->title }}</small>
            </div>
            {{ Form::open(['action' => ['Admin\TypeController@update', $type->id], 'method' => 'PATCH']) }}
            <div class="col-12">
                <div class="form-group row m-t-20">
                    <label for="categoryTitle" class="col-1 col-form-label">Naam</label>
                    <div class="col-11">
                        {!! Form::text('title', $type->title, ['id' => 'categoryTitle', 'class' => 'form-control', 'placeholder' => 'Naam van het artikel']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-1 col-form-label">Actief?</label>
                    <div class="col-11">
                        Nee
                        <div class="col-10 switch switch-success d-inline m-r-10">
                            <input name="is_active" value="1" type="checkbox" id="switch-s-1" @if ($type->is_active) checked @endif>
                            <label for="switch-s-1" class="cr"></label>
                        </div>
                        Ja
                    </div>
                </div>
                @if ($errors->any())
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <div class="offset-1 col-11">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.type.index') }}">Annuleren</a>
                        <button type="submit" class="btn btn-success">Opslaan</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('extra-css')

@endsection

@section('extra-js')


@endsection
