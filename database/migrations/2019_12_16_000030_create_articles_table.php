<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            // Create table columns
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->string('title')->unique();
            $table->boolean('published')->default(false);
            $table->dateTime('published_at')->useCurrent(true);
            $table->longText('content');
            $table->string('slug')->unique()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';

            // Set foreign keys for articles table
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
