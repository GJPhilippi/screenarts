<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            // Create table columns
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('tagline')->nullable();
            $table->string('status')->nullable();
            $table->string('country')->nullable();
            $table->integer('runtime')->nullable();
            $table->string('trailer')->nullable();
            $table->integer('id_trakt')->unique()->nullable();
            $table->integer('id_tmdb')->unique()->nullable();
            $table->string('id_imdb')->unique()->nullable();
            $table->string('slug')->unique()->nullable();
            $table->mediumText('overview')->nullable();
            $table->boolean('is_processed')->default(false);
            $table->date('released_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
