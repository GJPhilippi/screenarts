<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateShowGenresTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('show_genres', function (Blueprint $table) {
            $table->unsignedBigInteger('show_id');
            $table->unsignedBigInteger('genre_id');

            $table->engine = 'InnoDB';

            // Create foreign keys
            $table->foreign('show_id')
                ->references('id')->on('shows')
                ->onDelete('cascade');

            $table->foreign('genre_id')
                ->references('id')->on('genres');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('show_genres');
    }
}
